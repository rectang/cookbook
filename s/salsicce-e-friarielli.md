Salisicce e Friarielli
======================

Sausage and Sautéed Broccoli Rabe

Ingredients
-----------

*   2 links of Italian sausage (1/2 to 1/4 pound)
*   1 lb of broccoli rabe
*   1 or 2 cloves of sliced or crushed garlic (for the pan)
*   1 tbsp olive oil
*   salt and pepper to taste
*   chili flakes (optional)

Directions
----------

Prepare the broccoli rabe by cutting the leaves, stems, and florets into
about 2-3 inch chunks.  If you're interested in the best presentation, I
suggest using a bit more brocolli rabe and removing the thicker stems.
If you decide to cook the stems, taste them before cooking to make sure
they're not too tough (Tough stems happens rarely, but it does happen).
Put the broccoli rabe into a colander, rinse.

Brown the garlic in the olive oil in a frying pan large enough to hold
the broccoli rabe.  After it's browned, remove the garlic.  Cook the
sausage (you may want to prick the sausages with a fork) in the
garlic-flavored olive oil over moderate heat.  When the sausages are
cooked remove them to a plate (you'll be putting them back in just a
minute).

Now put the broccoli rabe into the pan with the olive oil and
juices from the cooked sausage.  Put the sausages on top of the broccoli
rabe (pour the juices from the plate onto the broccoli rabe also).
Cover and cook over moderate heat for a few minutes, then lower the heat
to medium low.  How long to cook this depends on how you like your
sautéed greens.  I like mine fairly done so I cook the dish so that the
greens get about 20 minutes total cooking time.

Check the broccoli rabe after about 5 minutes.  If the pan appears to be
too dry (happens rarely), add some white wine or water just to deglaze
the pan, and so that there will be a small amount of sauce when it's
done (see serving suggestion below).

Uncover, and serve directly from the frying pan.  I put the frying pan
on the table, so that people can dip bread into the sauce.

Notes
-----

(From Rusty)

This is a Neapolitan dish  learned in Naples around 1970 (both
r's in the word friarielli, which is Neapolitan dialect, are rolled).
This recipe serves 2.

Ada Boni (Italian chef) notes that if you are using hot Italian sausage,
you may not need the pepper.

Some recipes call for chili flakes.  I never use them.

The rumor is that 95% of broccoli rabe in the US is grown by Andy Boy
Farms in the Salinas valley, and they grow a good product.

Serve with a Chianti or GSM.  If you cooked hot Italian sausage, go for
a Zinfandel or whatever wine you like to serve with BBQ.

Tags
----

rusty


