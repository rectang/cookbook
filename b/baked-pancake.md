Baked Pancake
=============

Ingredients
-----------

* pancake batter

Directions
----------

Grease a 13x18 cookie sheet. Preheat oven to 475°F. Pour in batter and bake
until toothpick-test done, 15-18 minutes.

Notes
-----

Consider any of the usual modifications to pancakes: berries, maple syrup,
different batters, etc.

Tags
----

idea