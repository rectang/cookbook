Billy's Lunch Bread
===================

Ingredients
-----------

* 2 Tbsp shortening
* 2 tsp salt
* 1/4 cup brown sugar
* 1/2 cup condensed milk
* 1/2 cup hot water
* 1 cup warm water
* 1 package yeast
* 5-6 cups flour

Directions
----------

Stir together the shortening, salt, brown sugar, condensed milk, and hot
water.  Disolve the yeast in the warm water and combine.

Add 2-3 cups of flour and beat.  Add the remaining flour and knead.

Let rise for an hour or more, shape, let rise, bake 50 minutes at 400 degrees.

Notes
-----

Alternatly substitute water for the condensed milk, and then add either 3
Tbsp Starlac or 1/3 cup Carnation along with the first batch of flour.
