Goat Stew
=========

Ingredients
-----------

* goat meat (kid)
* random meat bones
* carrots
* celery
* onions
* garlic
* bay leaves
* salt
* pepper
* olive oil
* balsamic vinegar (or some acid)
* rosemary
* greens with stems removed (e.g. kale)

* hearty toast with melting cheese

Directions
----------

Brown goat using olive oil in pan.

Add vinegar and water.  Add spices.  Simmer for a long time.  Add vegetables
as they are ready.  Chop vegetables coarsely.

Let cool.  Check fat content, skim if necessary.

Put the cheesy toast on the bottom of a bowl and ladle stew over the top.


Notes
-----

The acid is important to tenderize the meat and leach the bones.

Use whatever you have lying around.

Consider skimming the fat if there's a big thick layer.  Goat is a very fatty
meat and there may be too much.  On the other hand, a thick layer of fat helps
to preserve it.

A packet of oxtail soup or similar can be used to substitute for the salt and
other seasonings.

Tags
----

lspoon
crockpot

