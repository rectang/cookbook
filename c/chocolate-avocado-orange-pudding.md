Chocolate Avocado Orange Pudding
================================

Ingredients
-----------

*   2 large avocados
*   1/4 cup (or more) unsweetened cocoa powder
*   1 teaspoon vanilla
*   1/4 cup agave syrup
*   1/4 cup maple syrup
*   salt
*   3 tablespoons frozen orange juice concentrate
*   Rose's sweetened lime juice (optional)
*   warm water

Directions
----------

Combine all ingredients and blend.

Notes
-----

Add water only to achieve desired consistency.

A possible ubstitution for the maple syrup: 1 teaspoon maple "flavoring" and
1/4 cup dark Karo syrup.

Tags
----

lspoon, dessert

