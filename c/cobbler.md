Cobbler (Mom's version)
=======================

Ingredients
-----------

Fruit (fresh, canned, or frozen):

*   (optional) apples
*   (optional) peaches
*   (optional) pears
*   (optional) pineapple
*   (optional) strawberries
*   (optional) raspberries
*   (optional) blackberries
*   (optional) blueberries
*   (optional) lemon juice
*   (optional) lime juice

Batter:

*   2 1/4 cups Bisquick
*   1 tablespoon oil
*   1 cup milk

Topping:

*   2/3 stick butter
*   1/2 cup brown sugar
*   1 tsp cinnamon
*   (optional) 1/8 tsp. cloves
*   (optional) 1/4 tsp. nutmeg
*   (optional) 1/4 tsp. coriander
*   (optional) 1/4 tsp. cardomom
*   (optional) 1/4 tsp. ginger

Directions
----------

Preheat oven to 400°F.

Grease 9x13 glass baking pan with butter.

Melt together topping ingredients in small saucepan.

Prepare fruit: sliced fresh apples or pears; drained, canned
sliced pears, peaches, or pineapple; any berries you have - frozen,
canned or fresh.  Avoid bananas.  Thaw frozen fruit before use.
Add lemon or lime juice if everything is bland.  Layer fruit so that it
fills the lower half of the pan.

Combine and lightly beat biscuit batter.  (These proportions are similar
to stock Bisquick biscuits, but with a bit more milk to make the batter
easier to work with.)  Dollop blobs of dough on top of the fruit layer.
Drizzle topping on top of the biscuit batter.

Bake at 400°F for 20 minutes until the cobbler biscuit is light brown
and the fruit filling bubbles all around.

Notes
-----

Louisiana Fish Fry Cobbler mix is not a good match for this recipe, as it is
more liquid and is intended to go _underneath_ the fruit, and to be used with
less fruit and less liquid.  When we pour it on top, it seeps in between the
fruit and gets soaked by the fruit juice, yielding a single doughy-fruity
layer.
