Corn Casserole
==============

Ingredients
-----------

- 8 tablespoons melted butter
- 8 ounce can cream-style corn
- 8 ounce can whole kernel corn
- 1 cup sour cream
- 2 eggs
- 1 box Jiffy Corn Muffin Mix

Optional ingredients:

- can of black beans (drained)
- garlic
- onion
- celery
- bell pepper

Instructions
------------

Preheat oven to 375 degrees.  Grease a 2-quart casserole dish.

Add butter and corn.  Mix in sour cream.  Beat eggs and stir in along with
corn muffin mix.

Bake 35 minutes.

Serve with butter.

Notes
-----

This is an adaptation of the original recipe which was published on the Jiffy
box.

Typical cans of corn will be 15 ounces, requiring that the recipe be
essentially doubled if you use the whole thing.

Substituting yogurt for sour cream makes the casserole too liquid.

Tags
----

lspoon
