Carbonara
=========

Ingredients
-----------

- Bacon or pancetta (any amount)
- Garlic
- 3 Eggs
- Grated romano cheese
- Black pepper
- Olive oil

- Pasta (e.g. spaghetti)

- Parsley

Preparation
-----------

Saute bacon/pancetta in skillet and remove from pan.  Pour off most of the
fat.  Saute the garlic.

Beat eggs together with grated romano and black pepper in a bowl.

Cook pasta and drain.  Retain a little of the pasta water.

Put pasta into skillet with bacon and garlic.

Stir pasta water into egg mixture, taking care not to cook the eggs.

Pour egg mixture into skillet and cook briefly until desired consistency is
achieved.

Serve with parsley for garnish.

Tags
----

lspoon
