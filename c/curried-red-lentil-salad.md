Curried Red Lentil Salad - Valerie Burke
========================================

My friend, Indigo Mahira, brought this to a potluck, and I could eat it all
day!  The night before you want to serve this, whish together the following
Vinaigrette:

3/4 cup olive oil
1/2 cup wine vinegar
1 tsp. ground cumin
1 tsp. dry mustard
1/2 tsp. cardamom
1/4 tsp. cayenne pepper
2 Tbsp. sugar
1/2 tsp. turmeric
1/4 tsp. ground cloves
2 tsp. salt (really?)
1/2 tsp. mace
1/4 tsp. ground nutmeg
2 tsp. pepper (wut?)
1/2 tsp. ground coriander
1/4 tsp. ground cinnamon (lol)

(The spice measurements are clearly off.)

Wash and cook 1 lb. red (or yellow) lentils in boiling water 5 to 6 minutes, or
until just tender.  Test after 2 minutes. (Note that 1 lb. dry red lentils is
a little more than 2 cups.)  Rinse and drain well. Combine with dressing and
let it sit overnight.

The next day, add:

1 cup black currants
1/3 cup capers
1 to 1-1/2 cups very finely chopped red or sweet onion, to taste.

Allow to sit, stirring (or inverting if in a sealed container) several times,
for at least 2 hours before serving.  This can be served cold or at room
temperature.
