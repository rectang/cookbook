Cornmeal Cake
=============

Ingredients
-----------

* 1 package corn muffin mix
* 1 egg
* 1/3 cup milk
* berries or cherries (canned, frozen...)
* butter
* granulated sugar

Directions
----------

Combine the egg and milk in a mixing bowl.  Stir in the corn muffin mix.

Grease a pie pan.  Pour in most of the cornmeal mix.  Gently layer in the
berries, then pour the remainder of the cornmeal mix over the center.

Bake 25% longer than the corn muffin mix recipe suggests.  Remove from the
oven and flip upside down.  Melt butter into the bottom of "pie", then
sprinkle a small amount of sugar.

Serve immediately.

Notes
-----

If you use cherry pie filling, separate out the cherries using a collander and
discard the corn-starchy snot.

Tags
----

dessert
