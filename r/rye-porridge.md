Rye Porridge
============

Ingredients
-----------

* 3 cups water
* raisins
* salt
* 1 tsp coriander
* 3/4 cup dark rye flour
* 1 tablespoon dark molasses
* milk/cream

Directions
----------

Add water.  Turn burner on high.  Add other ingredients.

Bring to a boil, then reduce heat and simmer.  Continue simmering after
thickening until the grittiness subsides, stirring to prevent sticking.

Remove from heat, stir in some milk/cream and serve.

Notes
-----

Add the flour before the water gets too hot, or the porridge will clump.
