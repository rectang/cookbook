Fried Oatmeal
=============

Ingredients
-----------

*   leftover cooked oatmeal
*   bacon grease

Directions
----------

Store excess cooked oatmeal in the fridge in a loaf pan.

Heat bacon grease.  Cut oatmeal using spatula into slices 2 cm thick.  Fry
both sides until lightly browned.

Spread butter on slices and serve.

Notes
-----

Possible future experiments: fry in butter, spread jam.  Try with sprinkled
brown sugar.

Using bacon grease is the original Spooner recipe, from Nonna.  During the
week she would make oatmeal every morning for the family.  On the weekends,
she'd fry up the leftovers.

Tags
----

lspoon, nonna, breakfast

