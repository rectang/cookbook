Pumpkin Soup
============

A savory pumpkin soup with no cream.

Ingredients
-----------

*   24oz can cooked pumpkin
*   4 cups water or broth
*   2 tablespoons oil
*   2 medium onions, chopped
*   1 tablespoon diced garlic
*   2-4 oz summer sausage, diced
*   2 cc grated ginger
*   6 bay leaves
*   1 teaspoon tamari
*   1 tablespoon brown sugar
*   1/4 teaspoon powdered cayenne pepper

Optional garnishes:

*   chopped parsley
*   grated cheese

Directions
----------

In a large soup pot, saute the onions in the oil.  Add the diced summer
sausage when the onions are getting done.  Add the garlic, saute until golden,
then add water to arrest the saute.

Add everything else.  Simmer until melded — around 20-30 minutes.

Notes
-----

If omitting the summer sausage, add salt to taste.

The tamari could be substituted for salt or soy sauce.

The brown sugar could be any sweetener.  Only a little is needed.

Tags
----

pumpkin, soup, rectang
