Polenta Picante
===============

Savory corn meal mush.

Ingredients
-----------

*   1 tsp butter
*   1/2 tsp red pepper flakes
*   1/2 tsp chopped garlic
*   2 cups water
*   1/2 tsp salt
*   2/3 cup corn meal
*   1/4 cup shredded parmesan

Directions
----------

Saute the garlic and red pepper in the butter in a small saucepan until brown
and the butter starts to stick.  Add water and turn up heat.  Stir in corn
meal and salt.  Bring to a boil, then reduce heat and simmer until thickening.
Stir in parmesan until melted.  Serve.

Notes
-----

Depending on the coarseness of the corn meal grain, the simmering time can vary
widely.  With a fine grain like Alber's Corn Meal, the mush will be ready
nearly immediately after boiling; with a coarse-grained polenta, several
minutes may be required.

Tags
----

breakfast, cereal, rectang

