Egg Sandwich
============

Ingredients
-----------

* 1 egg
* 1/2 tbsp butter
* 2 slices of bread
* salt and pepper to taste

Directions
----------

Heat a small cast-iron skillet, melting butter.  Crack egg into skillet.
Puncture yolk.  When sufficiently firm, flip.

Butter bread.  Make a sandwich with the egg, seasoning with salt and pepper to
taste.

Notes
-----

Toasting the bread is optional and a common variant.

Any sandwich bread will work but for maximum authenticity, use Wonder bread.

Rusty Humphrey learned this recipe around age 10 and made it all the time as a
kid.

Tags
----

rusty, breakfast
