Jello
=====

Ingredients
-----------

Mandatory ingredients:

*   flavored gelatin package
*   water

Optional ingredients:

*   berries: strawberries, blueberries, blackberries
*   fresh fruit: lightly ripe bananas, apples, grapes
*   canned fruit: mandarin orangest, peaches, pears, pineapple
*   coconut
*   cottage cheese
*   yogurt
*   gelatin

Forbidden ingredients:

*   Cool Whip

Directions
----------

Add boiling water to dissolve the flavored gelatin (and optional additional
gelatin).  Add cold water.  Then add everything else.

Notes
-----

Cottage cheese is especially good with pineapple.

Cool Whip is not food.

Add gelatin for firmness.

Do _not_ use fresh kiwi or fresh pineapple, because they will dissolve the
gelatin and you'll end up with jello soup.

Tags
----

lspoon

